import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentAComponent } from './component-a/component-a.component';
import { ComponentBComponent } from './component-b/component-b.component';
import { CustomMainComponent } from './custom-main/custom-main.component';
import { CustomRoutingModule } from './custom-routing/custom-routing.module';
import { CustomDetailsComponent } from './custom-details/custom-details.component';

@NgModule({
  declarations: [ComponentAComponent, ComponentBComponent, CustomMainComponent, CustomDetailsComponent],
  imports: [
    CommonModule,
    CustomRoutingModule
  ]
})
export class CustomModule { }
