import {Routes} from '@angular/router';
import {CustomMainComponent} from './custom-main/custom-main.component';
import {ComponentAComponent} from './component-a/component-a.component';
import {ComponentBComponent} from './component-b/component-b.component';
import {CustomDetailsComponent} from "./custom-details/custom-details.component";

export const CUSTOM_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: CustomMainComponent,
    data: {title: 'My custom component!'},
    children: [
      {
        path: 'component-a',
        component: ComponentAComponent,
        children:[
          {
            path: ':id',
            component: CustomDetailsComponent
          }
        ]
      },
      {
        path: 'component-b',
        component: ComponentBComponent
      }
    ]
  }
];
