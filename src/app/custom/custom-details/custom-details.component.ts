import { Component, OnInit } from '@angular/core';
import {ComponentAService} from '../component-a/component-a.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-custom-details',
  templateUrl: './custom-details.component.html',
  styleUrls: ['./custom-details.component.scss']
})
export class CustomDetailsComponent implements OnInit {

  public product: {id:number, name: string, description: string};

  constructor( private _customFirstService: ComponentAService,
               private _activatedRoute: ActivatedRoute) {

  }
  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.product = this._customFirstService.getById(+id);
    })
  }
}
