import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-custom-main',
  templateUrl: './custom-main.component.html',
  styleUrls: ['./custom-main.component.scss']
})
export class CustomMainComponent implements OnInit, OnDestroy {

  public title: string;
  public subscription : Subscription;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this._activatedRoute.data.subscribe((data: {title: string})=>{
      if (data){
        this.title = data.title;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription = null;
  }

}
