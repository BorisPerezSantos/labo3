import {Component, OnInit} from '@angular/core';
import {ComponentAService} from "./component-a.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-component-a',
  templateUrl: './component-a.component.html',
  styleUrls: ['./component-a.component.scss']
})
export class ComponentAComponent implements OnInit {

  public products: { id: number, name: string, description: string }[];

  constructor(private _customFirstService: ComponentAService,
              private _router: Router) {
    this.products = [];
  }

  ngOnInit() {
    this.products = this._customFirstService.products;
  }

  public navigate(id: number): void {
    this._router.navigate(['custom/component-a', id])
  }
}
