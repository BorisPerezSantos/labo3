import { TestBed } from '@angular/core/testing';

import { ComponentAService } from './component-a.service';

describe('ComponentAService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComponentAService = TestBed.get(ComponentAService);
    expect(service).toBeTruthy();
  });
});
