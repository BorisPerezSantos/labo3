import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeNGRoutingModule } from './prime-ng-routing.module';
import { PrimeNgMainComponent } from './prime-ng-main/prime-ng-main.component';
import { PrimeNgDataComponent } from './prime-ng-data/prime-ng-data.component';
import { PrimeNgListaComponent } from './prime-ng-lista/prime-ng-lista.component';
import { PrimeNgDetalleComponent } from './prime-ng-detalle/prime-ng-detalle.component';

@NgModule({
  declarations: [PrimeNgMainComponent, PrimeNgDataComponent, PrimeNgListaComponent, PrimeNgDetalleComponent],
  imports: [
    CommonModule,
    PrimeNGRoutingModule
  ]
})
export class PrimeNGModule { }
