import {Routes} from '@angular/router';
import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgDataComponent} from './prime-ng-data/prime-ng-data.component';
import {PrimeNgListaComponent} from './prime-ng-lista/prime-ng-lista.component';
import {PrimeNgDetalleComponent} from './prime-ng-detalle/prime-ng-detalle.component';

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    children: [
      {
        path: 'data',
        component: PrimeNgDataComponent
      },
      {
        path: 'lista',
        component: PrimeNgListaComponent,
        children: [
          {
            path: ':id',
            component: PrimeNgDetalleComponent
          }
        ]
      },
      {
        path: '**',
        redirectTo: '/prime-ng/data',
        component: PrimeNgDataComponent
      }
    ]
  }
];
