import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgDetalleComponent } from './prime-ng-detalle.component';

describe('PrimeNgDetalleComponent', () => {
  let component: PrimeNgDetalleComponent;
  let fixture: ComponentFixture<PrimeNgDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
