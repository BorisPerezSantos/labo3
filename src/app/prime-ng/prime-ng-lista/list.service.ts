import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private _detail: { id: number, detail: string }[];

  constructor() {
    this._detail = [
      {
        id: 1,
        detail: 'detail_1'
      },
      {
        id: 2,
        detail: 'detail_2',
      },
      {
        id: 3,
        detail: 'detail_3'
      },
      {
        id: 4,
        detail: 'detail_4'
      }];
  }

  public getById(id: number): { id: number, detail: string } {
    let response: { id: number, detail: string } = null;
    for (let detail of this._detail) {
      if (detail.id === id) {
        response = detail;
      }
    }
    return response;
  }

  get detail(): { id: number; detail: string }[] {
    return this._detail;
  }
}
