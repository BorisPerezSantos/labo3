import {Routes} from '@angular/router';
import {DashboardNotFoundComponent} from './dashboard/dashboard-not-found/dashboard-not-found.component';

export const ROUTES_CONFIG: Routes = [
  {
    path: 'custom',
    loadChildren: './custom/custom.module#CustomModule'
  },
  {
    path: 'ng-bootstrap',
    loadChildren: './ng-bootstrap/ng-bootstrap.module#NgBootstrapModule'
  },{
    path: 'prime-ng',
    loadChildren: './prime-ng/prime-ng.module#PrimeNGModule'
  },
  {
    path: ' ',
    redirectTo: '/custom',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: DashboardNotFoundComponent
  }
];


