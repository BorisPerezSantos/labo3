import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';
import {MainComponent} from './main/main.component';
import {HeaderComponent} from './header/header.component';
import {BodyComponent} from './body/body.component';
import {MenuListComponent} from './menu-list/menu-list.component';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {DashboardNotFoundComponent} from './dashboard-not-found/dashboard-not-found.component';
import {DashboardNavigationComponent} from './dashboard-navigation/dashboard-navigation.component';

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    BodyComponent,
    MenuListComponent,
    MenuItemComponent,
    DashboardNotFoundComponent,
    DashboardNavigationComponent],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    MainComponent
  ]
})
export class DashboardModule { }
