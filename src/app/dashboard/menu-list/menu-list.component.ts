import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuItem} from "../menu-item/menu-item";
import {Subscription} from "rxjs";
import {NavigationEnd, Router, Event} from "@angular/router";

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit, OnDestroy {

  public menu: MenuItem[];
  private subscription: Subscription;

  constructor(private _router: Router) {
    this.menu = [];
  }

  ngOnInit() {
    this._routerListener();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription = null;
  }

  private _routerListener(): void {
    this.subscription = this._router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this._showCorrespondingMenu();
      }
    })
  }

  private _showCorrespondingMenu(): void {
    if (this._router.isActive('/custom', false)) {
      this.menu = this._buildCustomMenu();
    }
    if (this._router.isActive('/ng-bootstrap', false)) {
      this.menu = this._buildNgBootstrapMenu();
    }
    if (this._router.isActive('/prime-ng', false)) {
      this.menu = this._buildPrimeNgMenu();
    }
  }

  private _buildCustomMenu(): MenuItem[] {
    return [
      {
        label: 'Component A',
        route: 'custom/component-a'
      },
      {
        label: 'Component B',
        route: 'custom/component-b'
      }
    ];
  }

  private _buildNgBootstrapMenu(): MenuItem[] {
    return [
      {
        label: 'Buttons',
        route:  '/ng-bootstrap/buttons'
      },
      {
        label: 'Alerts',
        route: '/ng-bootstrap/alerts'
      },
      {
        label: 'Collapse',
        route: '/ng-bootstrap/collapse'
      },
      {
        label: 'Dropdown',
        route: '/ng-bootstrap/dropdown'
      },
      {
        label: 'Modal',
        route: '/ng-bootstrap/modal'
      },
      {
        label: 'Tables',
        route: '/ng-bootstrap/table'
      },
      {
        label: 'Tooltip',
        route: '/ng-bootstrap/tooltip'
      },
      {
        label: 'Carousel',
        route: '/ng-bootstrap/carousel'
      }
      ]
  }

  private _buildPrimeNgMenu(): MenuItem[] {
    return [
      {
        label: 'Data',
        route: '/prime-ng/main'
      }
    ];
  }
}

